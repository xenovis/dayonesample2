package com.droidsonroids.bootcamp2.posts.edit;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;

import com.droidsonroids.bootcamp2.Constants;
import com.droidsonroids.bootcamp2.R;
import com.droidsonroids.bootcamp2.model.Post;
import com.droidsonroids.bootcamp2.posts.PostDetailsActivity;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class EditPostActivity extends PostDetailsActivity {

	@Override protected void handleSavedInstanceState(final Bundle savedInstanceState) {
		super.handleSavedInstanceState(savedInstanceState);
		if (savedInstanceState == null) {
			Post post = (Post) getIntent().getSerializableExtra(Constants.EXTRA_POST);
			mPostId = post.getObjectId();
			mPostTitleEditText.setText(post.getTitle());
			mPostBodyEditText.setText(post.getBody());
		}
	}

	@Override public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.activity_edit_post, menu);
		return true;
	}

	@Override public boolean onOptionsItemSelected(final MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_edit) {
			new AsyncTask<Void, Void, Boolean>() {
				@Override protected Boolean doInBackground(final Void... params) {
					try {
						Request request = buildPutRequest();
						Response response = mHttpClient.newCall(request).execute();
						return response.isSuccessful();
					} catch (IOException | JSONException e) {
						return false;
					}
				}

				@Override protected void onPostExecute(final Boolean success) {
					if (success) {
						setResult(RESULT_OK);
						finish();
					} else {
						Snackbar.make(mPostDetailsLinearLayout, R.string.error_request_put, Snackbar.LENGTH_SHORT).show();
					}
				}
			}.execute();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private Request buildPutRequest() throws JSONException {
		String url = buildPutRequestUrl(mPostId);
		RequestBody requestBody = RequestBody.create(JSON, buildJsonRequestBody());
		return getRequestBuilderWithHeaders()
				.url(url)
				.put(requestBody)
				.build();
	}

	private String buildJsonRequestBody() throws JSONException {
		JSONObject requestBody = new JSONObject();
		requestBody.put("title", mPostTitleEditText.getText().toString());
		requestBody.put("body", mPostBodyEditText.getText().toString());
		return requestBody.toString();
	}

	private String buildPutRequestUrl(final String objectId) {
		return POSTS_URL + "/" + objectId;
	}
}
